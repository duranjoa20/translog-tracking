import Foundation

// MARK: - Welcome
struct Welcome: Codable {
    let controller, action: String
    let param: Param
    let user: User
}

// MARK: - Param
struct Param: Codable {
    let id, name: JSONNull?
    let nick: String
    let token: JSONNull?
    let password: String
}

// MARK: - User
struct User: Codable {
    let id, name, nick, token: String
    let format, propietario, enterpriseID, puedeExportar: String
}

// MARK: - Encode/decode helpers

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }
    
    func hash(into hasher: inout Hasher) {
        
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}
