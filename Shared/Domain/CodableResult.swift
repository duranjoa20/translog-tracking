import Foundation

struct Response: Codable {
    let resultSet: ResultSet

    enum CodingKeys: String, CodingKey {
        case resultSet = "ResultSet"
    }
}

// MARK: - ResultSet
struct ResultSet: Codable {
    let userTracking: UserTracking

    enum CodingKeys: String, CodingKey {
        case userTracking = "User_Tracking"
    }
}

// MARK: - UserTracking
struct UserTracking: Codable {
    let id, name, nick, informeContable: String
}
