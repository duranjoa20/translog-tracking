import SwiftUI

struct LoginView: View {
    @ObservedObject var globalStore: GlobalStore
    @State private var isShowingDetailView = false
    @State private var showingAlert = false
    

    var body: some View {
            NavigationView {
                VStack(alignment: .leading, spacing: 10) {
                Image("Logo")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                TextField("Server", text: self.$globalStore.server)
                    .padding()
                    .background(Color.themeTextField)
                    .cornerRadius(20.0)
                    .shadow(radius: 10.0, x: 20, y: 10)
                TextField("Email", text: self.$globalStore.email)
                    .padding()
                    .background(Color.themeTextField)
                    .cornerRadius(20.0)
                    .shadow(radius: 10.0, x: 20, y: 10)
                
                SecureField("Password", text: self.$globalStore.password)
                    .padding()
                    .background(Color.themeTextField)
                    .cornerRadius(20.0)
                    .shadow(radius: 10.0, x: 20, y: 10)
                Button(action: {
                    globalStore.login()
                    UrlSessionManager().startLoad {
                        isShowingDetailView.toggle()
                    } onFailure: {
                        showingAlert.toggle()
                    }

                }) {
                    Text("Signin")
                        .frame(maxWidth: .infinity)
                        .padding()
                        .background(Color.blueTranslogColor)
                        .cornerRadius(20.0)
                        .foregroundColor(.white)
                        .shadow(radius: 10.0, x: 20, y: 10)
                    
                }
                NavigationLink(destination: ContentView().navigationBarHidden(true), isActive: $isShowingDetailView) {
                    EmptyView()
                }

                
                
            }
            .padding([.leading, .trailing], 25)
            .alert(isPresented: $showingAlert) {
                Alert(title: Text("Error"), message: Text("Credenciales incorrectas"), dismissButton: .default(Text("Ok")))
            }
        }
        

    }
}

fileprivate extension Color {
    static var themeTextField: Color {
        return Color(red: 220.0/255.0, green: 230.0/255.0, blue: 230.0/255.0, opacity: 1.0)
    }
    static var blueTranslogColor: Color {
        return Color(.sRGB, red: 0.00008419533842, green: 0.3863954544, blue: 0.580650568, opacity: 1)
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        
        return
            Group {
                LoginView(globalStore: GlobalStore())
                    .previewLayout(.fixed(width: 414, height: 500))
                    .padding()
                
                Preview {
                    LoginView(globalStore: GlobalStore())
                }
            }
    }
}
