import Foundation
import Combine

public class GlobalStore: ObservableObject {
    private var emailDefaultKey = "emailDefaultKey"
    private var serverDefaultKey = "serverDefaultKey"
    private var passwordDefaultKey = "passwordDefaultKey"
    static var response: Response?
    let defaults = UserDefaults.standard
    
    @Published var email: String = ""
    @Published var server: String = ""
    @Published var password: String = ""
    
    public init() {
        self.email = defaults.string(forKey: emailDefaultKey) ?? ""
        self.server = defaults.string(forKey: serverDefaultKey) ?? ""
        self.password = defaults.string(forKey: passwordDefaultKey) ?? ""
    }

    public func login() {
        saveDefaults()
    }
    
    private func saveDefaults() {
        let defaults = UserDefaults.standard
        defaults.setValue(self.email, forKey: emailDefaultKey)
        defaults.setValue(self.server, forKey: serverDefaultKey)
        defaults.setValue(self.password, forKey: passwordDefaultKey)
    }
    
    public func getServer() -> String {
        return defaults.string(forKey: serverDefaultKey) ?? ""
    }
    
    public func getPassword() -> String {
        return defaults.string(forKey: passwordDefaultKey) ?? ""
    }
    
    public func getEmail() -> String {
        return defaults.string(forKey: emailDefaultKey) ?? ""
    }
    
    
    public func getIndexPage() -> String {
        "\(defaults.string(forKey: serverDefaultKey) ?? "")\("/web/Tracking/principal.html")"
    }
    
    public func getIndexPageURL() -> URL? {
        URL(string: "\(defaults.string(forKey: serverDefaultKey) ?? "")\("/web/Tracking/principal.html")")
    }
}
