import SwiftUI

@main
struct Translog_TrackingApp: App {
    var body: some Scene {
        WindowGroup {
            LoginView(globalStore: GlobalStore())
        }
    }
}
