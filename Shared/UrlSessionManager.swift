import Foundation
import Alamofire

class UrlSessionManager  {
    func startLoad(onSuccess: @escaping  () -> Void, onFailure: @escaping  () -> Void) {
        
        let nick = GlobalStore().getEmail()
        let password = GlobalStore().getPassword()
        let server = GlobalStore().getServer()
        let request = server + "/4DACTION/W_Dispatcher"
        
        let login = Welcome(controller: "Login_Tracking", action: "in", param: Param(id: nil, name: nil, nick: nick, token: nil, password: password), user: User(id: "00000001", name: "Administrador", nick: "support@latam-soft.com", token: "CSupunaGWyupuqAxoVxoWHMSqUDQE3CCWFonkoUqiAzetsFdGD", format: "false", propietario: "MM", enterpriseID: "00000001", puedeExportar: "False"))

        AF.request(request,
                   method: .post,
                   parameters: login,
                   encoder: JSONParameterEncoder.default)
            .responseDecodable(of: Response.self) { (response) in
                guard let response2 = response.value
                else {
                    onFailure()
                    return
                }
                GlobalStore.response = response2
                onSuccess()
        }
    }
    
}
