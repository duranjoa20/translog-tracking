import Foundation
import SwiftUI

extension View {
    func debugAction(_ closure: () -> Void) -> Self {
        #if DEBUG
            closure()
        #endif

        return self
    }
}

extension View {
    func debugPrint(_ value: Any) -> Self {
        debugAction { print(value) }
    }
}

extension View {
    func debugShowSize(_ value: CGSize) -> some View {
        return debugBorder(
            "\(Int(value.width)) x \(Int(value.height))")
    }
    
    func debugBorder(_ msg: String = "") -> some View {
        #if DEBUG
            return overlay(
                Text(msg)
                    .fontWeight(.bold)
                    .padding(5)
                    .background(Color.black)
                    .foregroundColor(.white)
                    .border(Color.red, width: 3)
            ).border(Color.red, width: 3)
        #else
            return self
        #endif
    }
}


extension View {
    func measureSize(_ f: @escaping (CGSize) -> Void) -> some View {
        overlay(GeometryReader { proxy in
            Color.clear.preference(key: SizeKey.self, value: proxy.size)
        }
        .onPreferenceChange(SizeKey.self, perform: f))
    }
}

struct SizeKey: PreferenceKey {
    static let defaultValue: CGSize = .zero
    static func reduce(value: inout CGSize, nextValue: () -> CGSize) {
        value = nextValue()
    }
}
public struct Preview<Content: View>: View {
    let content: Content

    public init(@ViewBuilder _ content: () -> Content) {
        self.content = content()
    }

    public var body: some View {
        Group {
            self.content
                .previewDevice("iPhone 12 Pro")
                .previewDisplayName("Pro")

            self.content
                .previewDevice("iPhone 12 mini")
                .previewDisplayName("Mini")

            self.content
                .previewDevice("iPhone SE (1st generation)")
                .previewDisplayName("SE 1")
        
            self.content
                .previewDevice("iPad Air (4th generation)")
                .previewDisplayName("iPad")
        }
    }
}

